odds.js
=======

Fixed betting odds parser and formatter. 
Supports decimal, fractional and moneyline/american style odds.

[![Build Status](https://drone.io/bitbucket.org/ocho/odds.js/status.png)](https://drone.io/bitbucket.org/ocho/odds.js/latest)

Installation
------------

    npm install odds --save

Usage
-----

    var odds = require('odds');

    odds(5, 2).format('decimal'); // "3.50"
    odds(100).format('moneyline'); // "+10,000"

Parsing
-------

Odds default to "0/1" and may be passed in as strings or numbers.

As parameters:

    odds(num, [den]);

As an array:

    odds([num, den]);

As an object:

    odds({num: num, den: den});

As a string:

    odds('2/3');
    odds('+10,000');
    odds('2.50');

Formating
---------

Defaults to **decimal** if a format type is not specified.

Types:

* american (alias)
* decimal
* fractional
* moneyline

Methods
-------

**config({opts})** - Get or set configuration options.

**frac({num, den})** - Get or set the fractional odds.

**num(num)** - Get or set the odds numerator.

**den(num)** - Get or set the odds denominator.

**parse(val)** - Parse options or string to set the odds.

**format()** - Format the odds as a string.

Global Settings
---------------

You may configure options globally. If your data always represents
odds as an object with numerator/denumerator properties then configure
the library to use those properties by default. e.x:

    var Odds = require('odds');

    var odds = odds
      .global()
      .config({num: 'numerator'})
      .config({den: 'denominator'});

    var data1 = {numerator: 2, denominator: 3};
    var data2 = {numerator: 4, denominator: 1};
    odds(data1).format('fractional'); // "2/3"
    odds(data2).format('fractional'); // "4/1"
