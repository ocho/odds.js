var lib = require('./');
var test = require('tape');

test('lib', function (t) {
  var odds = lib();
  t.plan(2);
  t.equal(typeof lib, 'function');
  t.equal(typeof odds, 'object')
});

test('global', function (t) {
  t.plan(3);
  t.equal(typeof lib.global, 'function');
  var glob = lib.global();
  t.equal(typeof glob, 'function');
  var odds = glob();
  t.equal(typeof odds, 'object');
});

test('config', function (t) {
  var odds = lib();
  t.plan(4);
  odds.config({num: 'foo'});
  t.equal(odds._config.num, 'foo');
  odds.config({den: 'bar'});
  t.equal(odds._config.den, 'bar');
  t.deepEqual(odds.config(), {num: 'foo', den: 'bar'});
  odds = lib.global({num: 'baz', den: 'qux'})();
  t.deepEqual(odds.config(), {num: 'baz', den: 'qux'});
});

test('parse', function (t) {
  var odds = lib();
  t.plan(1);
  t.equal(typeof odds.parse, 'function');
});

test('parseAmerican', function (t) {
  t.plan(2);
  var odds = lib();
  t.equal(typeof odds.parseAmerican, 'function');
  t.equal(odds.parseAmerican, odds.parseMoneyline);
});

test('parseDecimal', function (t) {
  t.plan(3);
  var odds = lib();
  t.equal(typeof odds.parseDecimal, 'function');
  odds.parseDecimal('1.50');
  t.equal(odds._f.n, 1);
  t.equal(odds._f.d, 2);
});

test('parseFractional', function (t) {
  t.plan(3);
  var odds = lib();
  t.equal(typeof odds.parseFractional, 'function');
  odds.parseFractional('1/2');
  t.equal(odds._f.n, 1);
  t.equal(odds._f.d, 2);
});

test('parseMoneyline', function (t) {
  t.plan(5);
  var odds = lib();
  t.equal(typeof odds.parseMoneyline, 'function');
  odds.parseMoneyline(400);
  t.equal(odds._f.n, 4);
  t.equal(odds._f.d, 1);
  odds.parseMoneyline(-400);
  t.equal(odds._f.n, 1);
  t.equal(odds._f.d, 4);
});

test('parse params', function (t) {
  t.plan(6);
  var odds = lib();
  odds.parse(5, 2);
  t.equal(odds._f.n, 5);
  t.equal(odds._f.d, 2);
  odds = lib(2, 3);
  t.equal(odds._f.n, 2);
  t.equal(odds._f.d, 3);
  odds = lib(' 4 ');
  t.equal(odds._f.n, 4);
  t.equal(odds._f.d, 1);
});

test('parse array', function (t) {
  t.plan(6);
  var odds = lib();
  odds.parse([5, 2]);
  t.equal(odds._f.n, 5);
  t.equal(odds._f.d, 2);
  odds = lib([2, 3]);
  t.equal(odds._f.n, 2);
  t.equal(odds._f.d, 3);
  odds = lib([4, ' 5 ']);
  t.equal(odds._f.n, 4);
  t.equal(odds._f.d, 5);
});

test('parse object', function (t) {
  t.plan(4);
  var odds = lib();
  odds.parse({num: 5, den: 2});
  t.equal(odds._f.n, 5);
  t.equal(odds._f.d, 2);
  odds = lib({num: 2, den: 3});
  t.equal(odds._f.n, 2);
  t.equal(odds._f.d, 3);
});

test('parse string', function (t) {
  t.plan(4);
  var odds = lib();
  odds.parse(' 5 / 2 ');
  t.equal(odds._f.n, 5);
  t.equal(odds._f.d, 2);
  odds = lib('2/3');
  t.equal(odds._f.n, 2);
  t.equal(odds._f.d, 3);
});

test('frac', function (t) {
  t.plan(4);
  var odds = lib();
  var frac = odds.frac();
  t.equal(typeof frac, 'object');
  t.equal(frac.num, 0);
  t.equal(frac.den, 1);
  odds.frac({num: 1, den: 2});
  t.deepEqual(odds.frac(), {num: 1, den: 2});
});

test('num', function (t) {
  t.plan(2);
  var odds = lib();
  t.equal(odds.num(), 0);
  odds.num(1);
  t.equal(odds.num(), 1);
});

test('den', function (t) {
  t.plan(2);
  var odds = lib();
  t.equal(odds.den(), 1);
  odds.num(1);
  odds.den(2);
  t.equal(odds.den(), 2);
});

test('formatDecimal', function (t) {
  t.plan(2);
  var odds = lib();
  t.equal(typeof odds.formatDecimal, 'function');
  t.equal(odds.formatDecimal(), '1.00');
});

test('formatFractional', function (t) {
  t.plan(2);
  var odds = lib();
  t.equal(typeof odds.formatFractional, 'function');
  t.equal(odds.formatFractional(), '0/1');
});

test('formatMoneyline', function (t) {
  t.plan(4);
  var odds = lib();
  t.equal(odds.formatMoneyline(), '0');
  var odds = lib(100, 1);
  t.equal(typeof odds.formatMoneyline, 'function');
  t.equal(odds.formatMoneyline(), '+10,000');
  odds = lib(1, 100);
  t.equal(odds.formatMoneyline(), '-10,000');
});

test('formatAmerican', function (t) {
  t.plan(2);
  var odds = lib();
  t.equal(typeof odds.formatAmerican, 'function');
  t.equal(odds.formatAmerican, odds.formatMoneyline);
});

test('format', function (t) {
  t.plan(6);
  var odds = lib();
  t.equal(typeof odds.format, 'function');
  t.equal(odds.format(), '0/1');
  t.equal(odds.format('american'), '0');
  t.equal(odds.format('decimal'), '1.00');
  t.equal(odds.format('fractional'), '0/1');
  t.equal(odds.format('moneyline'), '0');
});
